<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;

class KeyGenerateCommand extends Command
{
    protected $name = 'key:generate';

    protected $description = 'Set the application key';

    public function handle()
    {
        $key = $this->getRandomKey();

        if ($this->option('show')) {
            $this->line($key, 'comment');
            return;
        }

        $path = base_path('.env');

        if (!file_exists($path)) {
            $this->error('File ' . $path . ' not found.');
            return;
        }

        $envContent = file_get_contents($path);

        $keyPos = strpos($envContent, 'APP_KEY=');

        if ($keyPos === false) {
            file_put_contents($path, PHP_EOL . 'APP_KEY=' . $key, FILE_APPEND);
            $this->info('Application key [' . $key . '] set successfully.');
            return;
        }

        $keyLineEndPos = strpos($envContent, PHP_EOL, $keyPos);
        $keyLine = substr($envContent, $keyPos, $keyLineEndPos - $keyPos);

        file_put_contents($path, str_replace($keyLine, 'APP_KEY=' . $key, $envContent));

        $this->info('Application key [' . substr($keyLine, strlen('APP_KEY=')) . '] has been changed to [' . $key . '].');
    }

    /**
     * Generate a random key for the application.
     *
     * @return string
     */
    protected function getRandomKey()
    {
        return Str::random(32);
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['show', null, InputOption::VALUE_NONE, 'Simply display the key instead of modifying files.'],
        ];
    }
}