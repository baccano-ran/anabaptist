<?php

namespace App\Http\Controllers;

use App\Models\Organisations;
use App\Providers\HttpStatusCodes;
use Illuminate\Http\Request;

class OrganisationController extends Controller
{
    public function list()
    {
        $organisations = Organisations::all();

        return response()->json($organisations);
    }

    public function detail($id)
    {
        return response()->json(Organisations::find($id));
    }

    public function create(Request $request)
    {
        $organisation = Organisations::create($request->all());

        return response()->json($organisation, 201);
    }

    public function update($id, Request $request)
    {
        $organisation = Organisations::find($id);
        $organisation->update($request->all());

        return response()->json($organisation);
    }

    public function delete($id)
    {
        try {
            Organisations::findOrFail($id)->delete();
            return response()->json([
                'message' => trans('messages.delete.success'),
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => trans('delete.error'),
            ], HttpStatusCodes::HTTP_BAD_REQUEST);
        }
    }
}
