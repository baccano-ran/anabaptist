<?php

namespace App\Http\Controllers;

use App\Services\AuthService;
use Illuminate\Hashing\HashManager;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');

        $view = app('view');

        $hashManager = new HashManager(app());
        $h = $hashManager->make($password);
        $t = $hashManager->check($password, $h);

        return response()->json('success');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationException
     */
    public function register(Request $request)
    {
        $authService = new AuthService();
        $user = $authService->createByEmail($request);

        return response()->json($user->toArray());
    }
}