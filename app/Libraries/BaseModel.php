<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Factory;
use Illuminate\Validation\Validator;

class BaseModel extends Model
{
    public $rules = [];
    /** @var Validator */
    public $validator;

    public function validate(array $data = []): bool
    {
        if (empty($this->rules)) {
            return true;
        }

        $data = empty($data) ? $this->attributes : $data;

        /** @var Factory $builder */
        $builder = app('validator');
        $this->validator = $builder->make($data, $this->rules);

        return !$this->validator->fails();
    }
}