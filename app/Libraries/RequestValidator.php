<?php
namespace App\Libraries;

use Illuminate\Contracts\Validation\Factory;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class RequestValidator
{
    /** @var Request */
    public $request;
    public $rules = [];
    public $messages = [];
    public $customAttributes = [];
    /** @var Validator */
    public $validator;

    public function __construct(Request $request, array $rules, array $messages = [], array $customAttributes = [])
    {
        $this->request = $request;
        $this->rules = $rules;
        $this->messages = $messages;
        $this->customAttributes = $customAttributes;
    }

    /**
     * @return array
     * @throws ValidationException
     */
    public function validate()
    {
        $this->createValidator();

        if ($this->validator->fails()) {
            $this->throwValidationException($this->validator);
        }

        return $this->extractInputFromRules($this->request, $this->rules);
    }

    /**
     * @param $validator
     * @throws ValidationException
     */
    protected function throwValidationException($validator)
    {
        $messages = $this->validator->errors()->getMessages();

        throw new ValidationException(
            $validator,
            new JsonResponse($messages, 422)
        );
    }

    protected function extractInputFromRules(Request $request, array $rules): array
    {
        return $request->only(collect($rules)->keys()->map(function ($rule) {
            return Str::contains($rule, '.') ? explode('.', $rule)[0] : $rule;
        })->unique()->toArray());
    }

    public function createValidator(): Validator
    {
        return $this->validator = $this
            ->getValidationFactory()
            ->make(
                $this->request->all(),
                $this->rules,
                $this->messages,
                $this->customAttributes
            );
    }

    protected function getValidationFactory(): Factory
    {
        return app('validator');
    }
}