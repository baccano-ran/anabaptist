<?php
namespace App\Services;

use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class AuthService
{
    public function loginByEmail(string $email, string $password): User
    {
    }

    /**
     * @param Request $request
     * @return User
     * @throws ValidationException
     */
    public function createByEmail(Request $request): User
    {
        $user = new User($request->all());

        if (!$user->validate()) {
            throw new ValidationException(
                $user->validator,
                new JsonResponse($user->validator->errors()->getMessages(), 422)
            );
        }

        $user->public_id = User::generatePublicId();

        $user->role = User::ROLE_ORGANIZER;
        $user->password_hash = User::getPasswordHash($request->input('password'));
        $user->auth_token = User::generateAuthToken();
        $user->verified = User::VERIFIED;
        $user->enabled = User::ENABLED;

        $user->save();

        return $user;
    }
}