<?php

namespace App\Models;

use App\Libraries\BaseModel;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Hashing\HashManager;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * App\User
 *
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static Builder|User query()
 * @mixin \Eloquent
 *
 * @property int $id
 * @property string $public_id
 * @property int $role
 * @property string $password_hash
 * @property string $auth_token
 * @property int $verified
 * @property int $enabled
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $address
 * @property string $city
 * @property string $country
 * @property string $phone
 * @property string $description
 */
class User extends BaseModel implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    const ROLE_VOLUNTEER = 1;
    const ROLE_ORGANIZER = 2;
    const ROLE_ADMIN = 3;

    const NOT_VERIFIED = 0;
    const VERIFIED = 1;

    const DISABLED = 0;
    const ENABLED = 1;

    protected $table = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'public_id',
        'role',
        'password_hash',
        'auth_token',
        'verified',
        'enabled',
        'first_name',
        'last_name',
        'email',
        'address',
        'city',
        'country',
        'phone',
        'description',
    ];

    public $rules = [
        'email' => 'required|email',
//        'password' => 'required|string|min:8',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public static function getPasswordHash(string $password): string
    {
        $hashManager = new HashManager(app());
        return $hashManager->make($password);
    }

    public static function generatePublicId(): string
    {
        return base64_encode(str_shuffle((string) microtime()));
    }

    public static function generateAuthToken(): string
    {
        return base64_encode(str_shuffle((string) microtime()));
    }

}
