<?php
use Laravel\Lumen\Routing\Router;

/**
 * @var Router $router
 */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/generate-key', function() {
    $appKey = str_random(32);
    config(['APP_KEY' => $appKey]);
    config(['app.locale' => 'en']);

    return 'New APP KEY has been generated: ' . $appKey;
});

$router->get('/organisation/get', 'OrganisationController@get');

$router->group(['prefix' => 'api/v1'], function() use ($router) {

    $router->post('auth/login', 'AuthController@login');
    $router->post('auth/register', 'AuthController@register');

});

$router->group(['prefix' => 'api/v1', 'middleware' => 'auth'], function() use ($router) {

    $router->get('organisations', 'OrganisationController@list');
    $router->get('organisations/{id}', 'OrganisationController@detail');
    $router->post('organisations', 'OrganisationController@create');
    $router->delete('organisations/{id}', 'OrganisationController@delete');
    $router->put('organisations/{id}', 'OrganisationController@update');

});