<?php

use App\Models\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {

            // "internal" numeric id
            $table->increments('id');
            // the alternate "external" string ID which we can use instead of numeric id
            $table->string('public_id')->nullable(false);

            // user role (volunteer organizer admin)
            $table->tinyInteger('role')->nullable(false);

            // password
            $table->string('password_hash')->nullable(false);

            // temporary authentication token
            $table->string('auth_token');

            // user states
            $table->boolean('verified')->nullable(false)->default(User::NOT_VERIFIED);
            $table->boolean('enabled')->nullable(false)->default(User::ENABLED);

            // data
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('address');
            $table->string('city');
            $table->string('country');
            $table->string('phone');
            $table->string('description');

            // created_at / updated_at
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
